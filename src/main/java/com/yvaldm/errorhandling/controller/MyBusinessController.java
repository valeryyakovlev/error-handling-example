package com.yvaldm.errorhandling.controller;

import com.yvaldm.errorhandling.exception.ConflictException;
import com.yvaldm.errorhandling.exception.SomeBusinessForbiddenException;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MyBusinessController {

  @GetMapping("/api/test/exception/conflict")
  public void performConflictingOp() {
    throw new ConflictException("conflict has occured"); // returns 409
  }

  @GetMapping("/api/test/exception/business")
  public void performBusinessExceptionOp() {
    throw new SomeBusinessForbiddenException("business exception has occured"); // returns 403
  }
}
