package com.yvaldm.errorhandling.controller;

import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;

import com.google.common.base.VerifyException;
import com.yvaldm.errorhandling.exception.ServiceException;
import javax.validation.ConstraintViolationException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageConversionException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@RestControllerAdvice
public class ErrorControllerAdvice extends ResponseEntityExceptionHandler {

  @ExceptionHandler(ServiceException.class)
  protected ResponseEntity<ServiceException> handleBusinessException(ServiceException ex) {
    logger.warn(ex.getMessage(), ex);
    return ResponseEntity.status(ex.getCode()).body(ex);
  }

  @Override
  protected ResponseEntity<Object> handleExceptionInternal(Exception ex, Object body,
      HttpHeaders headers, HttpStatus status, WebRequest request) {
    ServiceException bodyException =
        new ServiceException(status.value(), status.name(), ex.getMessage());

    logger.warn(ex.getMessage(), ex);
    return super.handleExceptionInternal(ex, bodyException, headers, status, request);
  }

  @ExceptionHandler({HttpMessageConversionException.class, VerifyException.class,
      ConstraintViolationException.class})
  protected ResponseEntity<ServiceException> handleParseException(Exception ex) {
    return handleBusinessException(
        new ServiceException(BAD_REQUEST.value(), BAD_REQUEST.name(), ex.getMessage())
    );
  }

  @ExceptionHandler(Exception.class)
  protected ResponseEntity<Object> handleUnhandledException(Exception ex, WebRequest webRequest) {
    try {
      return handleException(ex, webRequest);
    } catch (Exception e) {
      HttpHeaders headers = new HttpHeaders();
      return handleExceptionInternal(ex, null, headers, INTERNAL_SERVER_ERROR, webRequest);
    }
  }
}
