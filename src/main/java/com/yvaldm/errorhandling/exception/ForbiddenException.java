package com.yvaldm.errorhandling.exception;

public class ForbiddenException extends ServiceException {

  protected ForbiddenException(String reason, String message) {
    super(403, reason, message);
  }

  public ForbiddenException(String message) {
    this("FORBIDDEN", message);
  }

}
