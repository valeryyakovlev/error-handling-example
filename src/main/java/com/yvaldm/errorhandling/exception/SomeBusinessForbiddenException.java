package com.yvaldm.errorhandling.exception;

public class SomeBusinessForbiddenException extends ForbiddenException {

  public SomeBusinessForbiddenException(String message) {
    super("SOME_BUSINESS_FORBIDDEN", message);
  }

}
