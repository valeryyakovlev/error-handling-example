package com.yvaldm.errorhandling.exception;

public class ConflictException extends ServiceException {

  protected ConflictException(String reason, String message) {
    super(409, reason, message);
  }

  public ConflictException(String message) {
    this("CONFLICT", message);
  }

}
