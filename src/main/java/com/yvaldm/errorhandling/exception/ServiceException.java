package com.yvaldm.errorhandling.exception;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Objects;

@SuppressWarnings("NonFinalFieldOfException")
@JsonIgnoreProperties({"cause", "stackTrace", "localizedMessage", "suppressed", "stack_trace",
    "localized_message"})
public class ServiceException extends RuntimeException {

  private int code;

  private final String reason;

  private final String hostname;

  @JsonCreator
  protected ServiceException(String reason, String message) {
    this(-1, reason, message);
  }

  public ServiceException(int statusCode, String reason, String message) {
    super(Objects.requireNonNull(message));
    this.reason = Objects.requireNonNull(reason);
    this.code = statusCode;
    this.hostname = Objects.requireNonNull(getHostName());
  }

  public String getReason() {
    return reason;
  }

  public int getCode() {
    return code;
  }

  public String getHostname() {
    return hostname;
  }

  private static String getHostName() {
    try {
      return InetAddress.getLocalHost().getHostName();
    } catch (UnknownHostException e) {
      throw new RuntimeException(e);
    }
  }
}
