
# REST error handling



## Benefits

1. Controllers are clean (ResponseEntity is used only in Advice)
2. Consistency (same exceptions are processed same way producing same result)
3. Flexibility (any required exception can be created with whatever business codes and messages)
4. Unified error responses
5. It's runtime exception -> less code. 
The only processing with that checked exception is that it is catched and wrapped to response entity


## Examples


### Example 1

```
➜  ~ curl -i http://localhost:8080/api/test/exception/business
HTTP/1.1 403 
Content-Type: application/json;charset=UTF-8
Transfer-Encoding: chunked
Date: Sat, 24 Aug 2019 20:12:19 GMT

{"code":403,"reason":"SOME_BUSINESS_FORBIDDEN","hostname":"Valerijs-MacBook-Pro-2.local","message":"business exception has occured"}   
```


### Example 2


```

➜  ~ curl -i http://localhost:8080/api/test/exception/conflict
HTTP/1.1 409 
Content-Type: application/json;charset=UTF-8
Transfer-Encoding: chunked
Date: Sat, 24 Aug 2019 20:12:25 GMT

{"code":409,"reason":"CONFLICT","hostname":"Valerijs-MacBook-Pro-2.local","message":"conflict has occured"}
```
